import React from 'react'
import Pulse from 'pulse-framework'
import accounts from '../Collections/accounts.collection'

const core: Pulse = new Pulse({
    config: {
        framework: React
    },
    collections: {
        // A collection named "test"
        test: {
            groups: ['groupName', 'anotherGroupName'],
            data: {
                hi: true
            }
        },
        accounts
    }
})

console.log(core.test.hi)

export default core