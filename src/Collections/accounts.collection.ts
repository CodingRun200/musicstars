import Pulse from 'pulse-framework'

const accounts = new Pulse({
    request: {
        baseURL: 'https://us-central1-school-communication-app1.cloudfunctions.net/api',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
        }
    },
    routes: {
        newUser: (request, creds) => request.post('loginUser', creds)
    },
    data: {
        user: null
    },
    persist: ['user'],
    watch: {
        user({data, request}) {
            request.headers.Bearer = `Bearer ${data.user.uid}`
        }
    },
    actions: {
        newUser({ routes, data }, creds) {
            return routes.newUser(creds).then(res => (data.user.uid = res.user.uid))
        }
    }
})

accounts.base.newUser({ 
    email: "helloaccount210@gmail.com",
    password: "Vanossgames210"})

    console.log(accounts.base.user)
    console.log(accounts.base.newUser({ 
        email: "helloaccount210@gmail.com",
        password: "Vanossgames210"}))

export default accounts